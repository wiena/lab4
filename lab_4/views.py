from django.shortcuts import render

#Create your views here
def home(request):
    return render(request, 'story3.html')
def about(request):
	return render(request, 'story3about.html')
def contact(request):
	return render(request, 'story3contact.html')
def gallery(request):
	return render(request, 'story3gallery.html')
def schedule(request):
	return render(request, 'schedule.html')
def schedule_create(request):
	return render(request, 'schedule_create.html')