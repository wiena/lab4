from django.urls import path
from . import views

urlpatterns = [
    path(' ', views.schedule, name='schedule'),
    path('schedule/create', views.schedule_create, name='schedule_create'),
    path('schedule/clear', views.schedule_clear, name='schedule_clear'),
    path('schedule/delete/<int:id>', views.schedule_delete, name='schedule_delete'),
   ]
