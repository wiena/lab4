from django.db import models
from datetime import datetime

# Create your models here.


class Schedule(models.Model):
	
	category_choices = (('Akademis', 'Akademis'), ('Organisasi', 'Organisasi'), ('Kepanitiaan', 'Kepanitiaan'), ('Lainnya', 'Lainnya'))
	date = models.DateTimeField(default=datetime.now)
	time = models.TimeField()
	activity = models.CharField(max_length=30)
	place = models.CharField(max_length=20)
	category = models.CharField(max_length=20, choices=category_choices)
	
	def __str__(self):
		return self.activity
